/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9318103803377729, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.6040520260130064, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999644014097042, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9254894127047543, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.8357804021704437, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.8741758241758242, 500, 1500, "me"], "isController": false}, {"data": [0.8727148703956343, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.9676962312269765, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.9272410970118706, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.001594896331738437, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 58264, 0, 0.0, 256.7128415488105, 11, 11891, 115.0, 565.0, 996.9500000000007, 2155.980000000003, 192.61590542434743, 715.6918724875036, 251.0741806544805], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 1999, 0, 0.0, 748.4597298649322, 155, 2245, 701.0, 1211.0, 1408.0, 1724.0, 6.659958954129907, 4.20800140949419, 8.240398432502532], "isController": false}, {"data": ["getLatestMobileVersion", 28091, 0, 0.0, 52.92211028443258, 11, 1060, 29.0, 130.0, 174.0, 247.0, 93.62761599712027, 62.63175484182362, 69.03208015412677], "isController": false}, {"data": ["findAllConfigByCategory", 5006, 0, 0.0, 298.5852976428288, 27, 1801, 200.0, 629.3000000000002, 885.0, 1230.8600000000006, 16.68305200539883, 18.86618576391782, 21.668417155449653], "isController": false}, {"data": ["getNotifications", 3133, 0, 0.0, 477.36610277689135, 66, 2276, 240.0, 1291.0, 1463.0, 1716.579999999998, 10.439575086302264, 88.60385456548143, 11.611988304002226], "isController": false}, {"data": ["getHomefeed", 159, 0, 0.0, 9467.339622641506, 4213, 11891, 9585.0, 10538.0, 10818.0, 11597.000000000004, 0.5256458997966842, 9.19315665962775, 2.6302828032795014], "isController": false}, {"data": ["me", 3640, 0, 0.0, 410.8857142857146, 64, 1854, 237.0, 1072.9, 1255.9499999999998, 1492.3600000000006, 12.129451908722542, 15.958470763314073, 38.43756976934048], "isController": false}, {"data": ["findAllChildrenByParent", 3665, 0, 0.0, 408.0117326057295, 51, 1888, 230.0, 1114.0, 1300.3999999999996, 1576.3600000000006, 12.213084161979154, 13.71586600222268, 19.512310243162005], "isController": false}, {"data": ["getAllClassInfo", 7058, 0, 0.0, 211.67016151884403, 17, 1427, 168.0, 404.0, 552.0, 946.8199999999997, 23.522589417834244, 13.598997007185421, 60.39149177684201], "isController": false}, {"data": ["findAllSchoolConfig", 4886, 0, 0.0, 305.94596807204215, 33, 1858, 217.0, 608.3000000000002, 868.0, 1241.0, 16.282107676517242, 355.08987171092093, 11.9412723291645], "isController": false}, {"data": ["getChildCheckInCheckOut", 627, 0, 0.0, 2390.575757575761, 1276, 3937, 2328.0, 3040.4, 3347.2000000000007, 3659.8400000000006, 2.082115721800117, 139.00359090763806, 9.580985626095849], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 58264, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
